package cpir2468MV.repository;

import cpir2468MV.model.InhousePart;
import cpir2468MV.model.Inventory;
import cpir2468MV.model.Part;
import cpir2468MV.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

class InventoryRepositoryTest {
    private InventoryRepository repo;
    private Inventory inventory;

    @BeforeEach
    void setUp() {
        inventory = mock(Inventory.class);
        repo = new InventoryRepository("data/testmock.txt");
        repo.setInventory(inventory);
    }

    @AfterEach
    void tearDown() {
        inventory = null;
        repo = null;
    }

    @Test
    void addPartAndProduct() {
        Part part = new InhousePart(1,"1",5,2,1,3,1);

        ObservableList<Part> list = FXCollections.observableArrayList();
        list.add(part);

        Mockito.doNothing().when(inventory).addPart(part);
        Mockito.when(inventory.getAllParts()).thenReturn(list);

        Mockito.verify(inventory, times(0)).addPart(part);

        repo.addPart(part);

        Mockito.verify(inventory, times(1)).addPart(part);
        Mockito.verify(inventory, times(1)).getAllParts();

        assertEquals(1, repo.getAllParts().size());

        Mockito.verify(inventory, times(2)).getAllParts();
    }

    @Test
    void deletePart() {
        Part part = new InhousePart(1,"1",5,2,1,3,1);

        ObservableList<Part> list = FXCollections.observableArrayList();

        Mockito.doNothing().when(inventory).addPart(part);
        Mockito.doNothing().when(inventory).deletePart(part);
        Mockito.when(inventory.getAllParts()).thenReturn(list);

        Mockito.verify(inventory, times(0)).addPart(part);
        Mockito.verify(inventory, times(0)).deletePart(part);

        repo.addPart(part);
        repo.deletePart(part);

        Mockito.verify(inventory, times(1)).addPart(part);
        Mockito.verify(inventory, times(1)).deletePart(part);
        Mockito.verify(inventory, times(2)).getAllParts();

        assertEquals(0, repo.getAllParts().size());

        Mockito.verify(inventory, times(3)).getAllParts();
    }
}