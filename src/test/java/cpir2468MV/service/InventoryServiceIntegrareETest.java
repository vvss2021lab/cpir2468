package cpir2468MV.service;

import cpir2468MV.model.InhousePart;
import cpir2468MV.model.Inventory;
import cpir2468MV.model.Part;
import cpir2468MV.repository.InventoryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class InventoryServiceIntegrareETest {
    private InventoryService service;
    private InventoryRepository repository;

    @BeforeEach
    void setUp() {
        repository = new InventoryRepository("data/testmock.txt");
        service = new InventoryService(repository);
    }

    @AfterEach
    void tearDown() throws IOException {
        repository = null;
        service = null;
        new FileWriter("D:\\Facultate\\Anul3\\Semestrul2\\VVSS\\cpir2468\\target\\classes\\data\\testmock.txt", false).close();
    }

    @Test
    void addInhousePart() {
        service.addInhousePart("1", 5, 2, 1,3, 1);
        assertEquals(1, service.getAllParts().size());
    }

    @Test
    void updateInhousePart(){
        service.addInhousePart("1", 5, 2, 1,3, 1);
        assertEquals(1, service.getAllParts().size());
        service.updateInhousePart(0, 1,"2",2,3,1,5,1);
        assertEquals(service.lookupPart("2").getPartId(), 1);
    }
}