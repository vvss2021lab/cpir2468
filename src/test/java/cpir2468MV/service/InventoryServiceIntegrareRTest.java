package cpir2468MV.service;

import cpir2468MV.model.InhousePart;
import cpir2468MV.model.Inventory;
import cpir2468MV.repository.InventoryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

class InventoryServiceIntegrareRTest {
    private InventoryService service;
    private InventoryRepository repository;
    private Inventory inventory;

    @BeforeEach
    void setUp() {
        inventory = mock(Inventory.class);
        repository = new InventoryRepository("data/testmock.txt");
        repository.setInventory(inventory);
        service = new InventoryService(repository);
    }

    @AfterEach
    void tearDown() {
        inventory = null;
        repository = null;
        service = null;
    }

    @Test
    void addInhousePart() {
        InhousePart part = new InhousePart(0,"1",5,2,1,3,1);

        Mockito.when(inventory.lookupPart("1")).thenReturn(part);

        service.addInhousePart("1", 5, 2, 1, 3, 1);

        Mockito.verify(inventory, times(0)).lookupPart("1");

        assertEquals(0, service.lookupPart("1").getPartId());

        Mockito.verify(inventory, times(1)).lookupPart("1");
    }

    @Test
    void deletePart() {
        InhousePart part = new InhousePart(0,"1",5,2,1,3,1);

        Mockito.when(inventory.lookupPart("1")).thenReturn(null);
        Mockito.doNothing().when(inventory).deletePart(part);

        service.addInhousePart("1", 5, 2, 1, 3, 1);

        Mockito.verify(inventory, times(0)).deletePart(part);

        service.deletePart(part);

        Mockito.verify(inventory, times(1)).deletePart(part);

        Mockito.verify(inventory, times(0)).lookupPart("1");

        assertNull(service.lookupPart("1"));

        Mockito.verify(inventory, times(1)).lookupPart("1");

    }
}